package genericCheckpointing.driver;

import genericCheckpointing.util.ProxyCreator;
import genericCheckpointing.util.MyAllTypesFirst;
import genericCheckpointing.util.MyAllTypesSecond;
import genericCheckpointing.xmlStoreRestore.StoreRestoreHandler;
import genericCheckpointing.server.StoreRestoreI;
import genericCheckpointing.server.StoreI;
import genericCheckpointing.server.RestoreI;
import genericCheckpointing.util.SerializableObject;
import java.util.*;
// import the other types used in this file

public class Driver {
    
    public static void main(String[] args) throws Exception{
	
	// FIXME: read the value of checkpointFile from the command line
	String mode = args[0]; //serdeser or deser
	int NUM_OF_OBJECTS = Integer.parseInt(args[1]);
	String checkpointFile = args[2];
	int flag = 0;	
	//System.out.println("1");
	//do validation here
	
	
	ProxyCreator pc = new ProxyCreator();
	
	// create an instance of StoreRestoreHandler (which implements
	// the InvocationHandler
	StoreRestoreHandler handler = new StoreRestoreHandler();
	
	// create a proxy
	StoreRestoreI cpointRef = (StoreRestoreI) pc.createProxy(
								 new Class[] {
								     StoreI.class, RestoreI.class
								 }, 
								 new StoreRestoreHandler()
								 );
		
	// FIXME: invoke a method on the handler instance to set the file name for checkpointFile and open the file
	handler.setFileName(checkpointFile);
	//System.out.println("Set");
	handler.openFile(checkpointFile);
	//System.out.println("Open");
	MyAllTypesFirst myFirst;
	MyAllTypesSecond mySecond;

	// Use an if/switch to proceed according to the command line argument
	// For deser, just deserliaze the input file into the data structure and then print the objects
	
	
	// The code below is for "serdeser" mode
	if(mode.equals("serdeser")) {
		//write first
		//read second
	
	// For "serdeser" mode, both the serialize and deserialize functionality should be called.

	// create a data structure to store the objects being serialized
	Vector <Object> ser = new Vector<Object>();
        // NUM_OF_OBJECTS refers to the count for each of MyAllTypesFirst and MyAllTypesSecond
	for (int i=0; i<NUM_OF_OBJECTS; i++) {

	    // FIXME: create these object instances correctly using an explicit value constructor
	    // use the index variable of this loop to change the values of the arguments to these constructors
	    myFirst = new MyAllTypesFirst(i);
	    mySecond = new MyAllTypesSecond(i);

	    // FIXME: store myFirst and mySecond in the data structure
	    
	    ser.add(myFirst);
	    ser.add(mySecond);
	    ((StoreI) cpointRef).writeObj(myFirst, "XML");
	    ((StoreI) cpointRef).writeObj(mySecond, "XML");
	}
	flag = 1;
}
	SerializableObject myRecordRet;
	
	
	// create a data structure to store the returned objects
	//vector to store deserialized objects
	if(mode.equals("deser") || flag == 1) {
	Vector<Object> deser = new Vector<Object>();
	//System.out.println("Here?");
	for (int j=0; j<2*NUM_OF_OBJECTS; j++) {
		//System.out.println("Do we get here");
	    myRecordRet = ((RestoreI) cpointRef).readObj("XML");
	    //handler.readObj("XML");
	    //System.out.println(myRecordRet);
	    // FIXME: store myRecordRet in the vector
	    deser.add(myRecordRet);
	}
	/*
	Iterator itr = deser.iterator();
	while(itr.hasNext()) {
		System.out.println(itr.next());
	}
	*/
	}
	// FIXME: invoke a method on the handler to close the file (if it hasn't already been closed)
	handler.close();
	// FIXME: compare and confirm that the serialized and deserialzed objects are equal. 
	// The comparison should use the equals and hashCode methods. Note that hashCode 
	// is used for key-value based data structures
    
    }
}
