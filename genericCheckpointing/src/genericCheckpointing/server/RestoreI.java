package genericCheckpointing.server;
import genericCheckpointing.util.SerializableObject;
import genericCheckpointing.server.StoreRestoreI;
public interface RestoreI extends StoreRestoreI {
	SerializableObject readObj(String wireFormat) throws Exception;
}
