//invokes createProxy

package genericCheckpointing.xmlStoreRestore;
import genericCheckpointing.util.SerializableObject;
import java.lang.Object;
import java.lang.reflect.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import genericCheckpointing.driver.Driver;
import genericCheckpointing.util.MyAllTypesFirst;
import genericCheckpointing.util.MyAllTypesSecond;
import java.io.BufferedReader;
import java.io.*;
import java.util.*;
import java.io.File;
import java.nio.file.Paths;
import java.nio.file.*;
import java.io.FileNotFoundException;

public class StoreRestoreHandler implements InvocationHandler {

	private String fileName;
	ArrayList<String> al = new ArrayList<String>();
	FileOutputStream fos = null;
	ObjectOutputStream oos = null;
	BufferedReader in = null;
	BufferedReader toFile = null;
	Class<?> newClass = null;
	Object result = null;
	Object obj = null;
	
	public void openFile(String fileName) throws FileNotFoundException, IOException{
		try {
			Path path = Paths.get(fileName);
			in = Files.newBufferedReader(path);
		}
		catch (FileNotFoundException e) {
			System.err.println("The error is: " + e);
			e.printStackTrace();
		}
		catch (IOException e) {
			System.err.println("The error is: " + e);
			e.printStackTrace();
		}
	}
			
	public void close() throws Exception{
		try {
			in.close();
		}
		catch (Exception e) {
			System.err.println("The error is: " + e);
			e.printStackTrace();
		}
	}
	
	
	public void setFileName(String fileIn) {
		this.fileName = fileIn;
	}
	
	public void writeObj(MyAllTypesFirst first, String format) throws Exception{
		
		//if first parameter is first
		//get the constructor and get the fields
		FileOutputStream fos = new FileOutputStream(fileName);
		PrintWriter pw = new PrintWriter(fos, true);
		pw.println("<DPSerialization>");
		pw.println("<complexType xsi:type=\"genericCheckpointing.util.MyAllTypesFirst\">");
		newClass = Class.forName("genericCheckpointing.util.MyAllTypesFirst");
		obj = newClass.newInstance();
		Class<?> MyAllTypesFirstClass = obj.getClass();
		Field[] fieldList = MyAllTypesFirstClass.getFields();
		//Constructor[] allConstructors = MyAllTypesFirstClass.getDeclaredConstructors();
		/*
		Class<?> [] argTypes = new Class<?>[4];
		argTypes[0] = int.class;
		argTypes[1] = long.class;
		argTypes[2] = String.class;
		argTypes[3] = boolean.class;
		Constructor constructor = MyAllTypesFirstClass.getDeclaredConstructor(argTypes);
		*/
		for(int i = 0; i < fieldList.length; i++) {
			Class fieldClass = fieldList[i].getType();
			String fieldName = fieldList[i].getName();
			Object fieldObject = fieldList[i].get(obj);
			if(fieldClass == int.class) {
				//get field name
				if(fieldName.equals("myInt")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				//write line
				pw.println("<myInt xsi:type=\"xsd:int\">" + invokeRet.toString() + "</myInt>"); 
				}
			}
			if(fieldList[i].getType() == long.class) {
				//get field name
				if(fieldName.equals("myLong")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myLong xsi:type=\"xsd:long\">" + invokeRet.toString() + "</myLong>");
				
				}
			}
			if(fieldList[i].getType() == String.class) {
				//get field name
				if(fieldName.equals("myString")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myString xsi:type=\"xsd:string\">" + invokeRet.toString() + "</myString>");
				}
			}
			if(fieldList[i].getType() == boolean.class) {
				//get field name
				if(fieldName.equals("myBool")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myBool xsi:type=\"xsd:boolean\">" + invokeRet.toString() + "</myBool>");
			}
			}

			
	}
	pw.println("</complexType>");
			pw.println("</DPSerialization>");
}
	
	public void writeObj(MyAllTypesSecond second, String format) throws Exception {
		FileOutputStream fos = new FileOutputStream(fileName);
		PrintWriter pw = new PrintWriter(fos, true);
		pw.println("<DPSerialization>");
		pw.println("<complexType xsi:type=\"genericCheckpointing.util.MyAllTypesSecond\">");
		newClass = Class.forName("genericCheckpointing.util.MyAllTypesSecond");
		obj = newClass.newInstance();
		Class<?> MyAllTypesFirstClass = obj.getClass();
		Field[] fieldList = MyAllTypesFirstClass.getFields();
		//Constructor[] allConstructors = MyAllTypesFirstClass.getDeclaredConstructors();
		/*
		Class<?> [] argTypes = new Class<?>[4];
		argTypes[0] = double.class;
		argTypes[1] = float.class;
		argTypes[2] = short.class;
		argTypes[3] = char.class;
		*/
		//Constructor constructor = MyAllTypesSecondClass.getDeclaredConstructor(argTypes);
		for(int i = 0; i < fieldList.length; i++) {
			Class fieldClass = fieldList[i].getType();
			String fieldName = fieldList[i].getName();
			//Object fieldObject = fieldList[i].get(obj);
			
			if(fieldList[i].getType() == double.class) {
				//get field name
				if(fieldName.equals("myDoubleT")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myDoubleT xsi:type=\"xsd:double\">" + invokeRet.toString() + "</myDoubleT>");
			}
			}
			if(fieldList[i].getType() == float.class) {
				//get field name
				if(fieldName.equals("myFloatT")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myFloatT xsi:type=\"xsd:float\">" + invokeRet.toString() + "</myFloatT>");
			}
			}
			if(fieldList[i].getType() == short.class) {
				//get field name
				if(fieldName.equals("myShortT")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myShortT xsi:type=\"xsd:float\">" + invokeRet.toString() + "</myFloatT>");
			}
			}
			if(fieldList[i].getType() == char.class) {
				//get field name
				if(fieldName.equals("myCharT")) {
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirstClass.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				pw.println("<myCharT xsi:type=\"xsd:char\">" + invokeRet + "</myCharT>");
			}
			}
		
	//write stuff to file
	}
	pw.println("</complexType>");
	pw.println("</DPSerialization>");	
}		
	public SerializableObject readObj(String format) throws Exception{
		//List<String> al = new ArrayList<String>();
		String line;
		String newLine;
		String line2;
		System.out.println("Reading?");
		while((line = in.readLine()) != null) {
			//FIXME parse for whitespaces in the front of the line
			//check if file is MyAllTypesFirst or MyAllTypesSecond
			line2 = line.trim();
			System.out.println(line2);
			//line2 = in.readLine();
			if(line2.startsWith("<complexType")) {
				newLine = line.substring(22);
				if(newLine.equals("genericCheckpointing.util.MyAllTypesFirst\">")) {
					System.out.println("First works");
					//we know this is MyAllTypesFirst
					//FIXME use reflection to create the object
					newClass = Class.forName("genericCheckpointing.util.MyAllTypesFirst");
					SerializableObject obj = (SerializableObject) newClass.newInstance();
					Method m[] = newClass.getDeclaredMethods();
					Method meth = null;
					/*
					Class<?> [] classArr = new Class<?>[4];
					classArr[0] = int.class;
					classArr[1] = long.class;
					classArr[2] = String.class;
					classArr[3] = boolean.class;
					*/
					//Object[] params = new Object[1];
					/*
					for(int i = 0; i < m.length; i++) {
						System.out.println(m[i].toString());	
					}
					*/
					
					
					//obj = new MyAllTypesFirst();
					if(line2.startsWith("<myInt")) {
						//obj.setInt(314);
						//System.out.println(m[0].toString());
						//get the method with reflection
						try {
						meth = newClass.getMethod("setInt", int.class); 
						}
						catch (Exception e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						//set the params
						//params[0] = new Integer(314);
						//invoke
						try {
							result = meth.invoke(obj, 314);
						}
						catch (Exception e) {
							System.out.println("Exception: " + e.getMessage());
							System.exit(1);
						}
					}
					if(line2.startsWith("<myLong")) {
						//obj.setLong(314159);
						//System.out.println(m[1].toString());
						try {
						meth = newClass.getMethod("setLong", long.class); 
						}
						catch (Exception e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						
						//params[1] = new Long(314159);
						try {
						result = meth.invoke(obj, 314159);
						}
						catch (Exception e) {
							System.out.println("Exception: " + e.getMessage());
							System.exit(1);
						}
					}
					if(line2.startsWith("<myString")) {
						//obj.setString("Design Patterns");
						//System.out.println(m[2].toString());
						try {
						meth = newClass.getMethod("setString", String.class); 
						}
						catch (Exception e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						try {
						//params[2] = new String("Design Patterns");
						result = meth.invoke(obj, "Design Patterns");
						}
						catch (Exception e) {
							System.out.println("Exception: " + e.getMessage());
							System.exit(1);
						}	
					}
					if(line2.startsWith("<myBool")) {
						//obj.setBool(false);
						//System.out.println(m[3].toString());
						try {
						meth = newClass.getMethod("setBool", boolean.class); 
						}
						catch (Exception e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						try {
						//params[3] = new Boolean(false);
						result = meth.invoke(obj, false);
						}
						catch (Exception e) {
							System.out.println("Exception: " + e.getMessage());
							System.exit(1);
						}
					}
					//return result;		
					
				}
				if(newLine.equals("genericCheckpointing.util.MyAllTypesSecond\">")) {
					System.out.println("Second works");
					//we know this is MyAllTypesSecond
					//FIXME use reflection to create the object
					newClass = Class.forName("genericCheckpointing.util.MyAllTypesSecond");
					SerializableObject obj = (SerializableObject)newClass.newInstance();
					//Method m[] = newClass.getDeclaredMethods();
					Method meth = null;
					/*
					Class<?> [] classArr = new Class<?>[4];
					classArr[0] = double.class;
					classArr[1] = float.class;
					classArr[2] = short.class;
					classArr[3] = char.class;
					*/
					//Object[] params = new Object[1];
					
					//obj = new MyAllTypesSecond();
					//read another line here? 
					if(line2.startsWith("<myDoubleT")) {
						//obj.setDouble(3.1459);
						//System.out.println(m[0].toString());
						//get the method with reflection
						try {
						meth = newClass.getMethod("setDoubleT", double.class); 
						}
						catch (NoSuchMethodException e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						//set the params
						try {
						//params[0] = new Double(3.1459);
						//invoke
						result = meth.invoke(obj, 3.1459);
						}
						catch (IllegalAccessException e) {
							System.out.println("Illegal access: " + e.getMessage());
							System.exit(1);
						}
						catch (InvocationTargetException e) {
							System.out.println("Invocation target 									exception: " + e.getMessage());
         						System.out.println("Exception: " + e.getTargetException().getMessage());
         						System.exit(1);
     						}
					}
					if(line2.startsWith("<myFloatT")) {
						//obj.setFloat(3145.9);
						//System.out.println(m[0].toString());
						//get the method with reflection
						try{
						meth = newClass.getMethod("setFloatT", float.class); 
						}
						catch (NoSuchMethodException e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						//set the params
						//params[0] = new Float(3145.9);
						//invoke
						try {
						result = meth.invoke(obj, 3145.9);
						}
						catch (IllegalAccessException e) {
							System.out.println("Illegal access: " + e.getMessage());
							System.exit(1);
						}
						catch (InvocationTargetException e) {
							System.out.println("Invocation target exception: " + e.getMessage());
         						System.out.println("Exception: " + e.getTargetException().getMessage());
         						System.exit(1);
     						}
					}
					if(line2.startsWith("<myShortT")) {
						//obj.setShort(314);
						//System.out.println(m[0].toString());
						//get the method with reflection
						try {
						meth = newClass.getMethod("setShortT", short.class); 
						}
						catch (NoSuchMethodException e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						//set the params
						//params[0] = new Short(314);
						//invoke
						try {
						result = meth.invoke(obj, 314);
						}
						catch (IllegalAccessException e) {
							System.out.println("Illegal access: " + e.getMessage());
							System.exit(1);
						}
						catch (InvocationTargetException e) {
							System.out.println("Invocation target 									exception: " + e.getMessage());
         						System.out.println("Exception: " + e.getTargetException().getMessage());
         						System.exit(1);
     						}
					}
					if(line2.startsWith("<myCharT")) {
						//obj.setChar("P");
						//System.out.println(m[0].toString());
						//get the method with reflection
						try {
						meth = newClass.getMethod("setCharT", char.class); 
						}
						catch (NoSuchMethodException e) {
							System.out.println("No such method: " + e.getMessage());
							System.exit(1);
						}
						//set the params
						//params[0] = new Char("P");
						//invoke
						try {
						result = meth.invoke(obj, 'P');
						}
						catch (IllegalAccessException e) {
							System.out.println("Illegal access: " + e.getMessage());
							System.exit(1);
						}
						catch (InvocationTargetException e) {
							System.out.println("Invocation target 									exception: " + e.getMessage());
         						System.out.println("Exception: " + e.getTargetException().getMessage());
         						System.exit(1);
     						}
					}
					//return result;
				}
			}
		}
		return (SerializableObject)result;
	}
	
	
		
	public Object invoke(Object proxy, Method m, Object [] args) {
		String methodName = m.getName();
		Class className = m.getDeclaringClass();
		if(className.equals(MyAllTypesFirst.class) || (className.equals(MyAllTypesSecond.class)) && methodName.equals("readObj")) {
			System.out.println("readObj method was called");
		}
		if(className.equals(MyAllTypesFirst.class) || (className.equals(MyAllTypesSecond.class)) && methodName.equals("writeObj")) {
			System.out.println("writeObj method was called");
		}
		return result;
	}
}	
