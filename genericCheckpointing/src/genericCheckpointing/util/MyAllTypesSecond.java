package genericCheckpointing.util;
import genericCheckpointing.util.SerializableObject;
public class MyAllTypesSecond extends SerializableObject {
	private double myDoubleT;
	private float myFloatT;
	private short myShortT;
	private char myCharT;
	
	public MyAllTypesSecond() {}
	public MyAllTypesSecond(int i) {
		this.myDoubleT = (double) i;
		this.myFloatT = (float) i;
		this.myShortT = (short) i;
		this.myCharT = Integer.toString(i).charAt(0);
	}
	public void setDoubleT(double doubleIn) {
		this.myDoubleT = doubleIn;
		System.out.println("setDouble was called" + doubleIn);
	}
	public void setFloatT(float floatIn) {
		this.myFloatT = floatIn;
		System.out.println("setFloat was called" + floatIn);
	}
	public void setShortT(short shortIn) {
		this.myShortT = shortIn;
		System.out.println("setShort was called" + shortIn);
	}
	public void setCharT(char charIn) {
		this.myCharT = charIn;
		System.out.println("setChar was called" + charIn);
	}
	
	public double getmyDoubleT() {
		return this.myDoubleT;
	}
	public float getmyFloatT() {
		return this.myFloatT;
	}
	public short getmyShortT() {
		return this.myShortT;
	}
	public char getmyCharT() {
		return this.myCharT;
	}
}
