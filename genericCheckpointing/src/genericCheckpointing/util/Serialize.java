/*package genericCheckpointing.util;
import java.util.ArrayList;
import java.util.*;
import java.io.*;
import java.lang.reflect.*;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
public class Serialize {
	public void writeObj(MyAllTypesFirst first, String format) {
		
		//if first parameter is first
		//get the constructor and get the fields
		al.add("<DPSerialization>");
		al.add("<complexType xsi:type=\"genericCheckpointing.util.MyAllTypesFirst\">");
		String line = "";
		String clsName = "MyAllTypesFirst";
		obj = clsName.newInstance();
		Class<?> MyAllTypesFirstClass = obj.getClass();
		Field<?>[] fieldList = MyAllTypesFirstClass.getFields();
		//Constructor[] allConstructors = MyAllTypesFirstClass.getDeclaredConstructors();
		Class<?> [] argTypes = new Class<?>[4];
		argTypes[0] = int.class;
		argTypes[1] = long.class;
		argTypes[2] = String.class;
		argTypes[3] = boolean.class;
		Constructor constructor = MyAllTypesFirstClass.getDeclaredConstructor(argTypes);
		for(int i = 0; i < fieldList.length; i++) {
			//Class fieldClass = fieldList[i].getType();
			String fieldName = fieldList[i].getName();
			Object fieldObject = fieldList[i].get(obj);
			if(fieldList[i].getType() == int.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				//write line
				line = line + "<myInt xsi:type=\"xsd:int\">" + invokeRet.toString() + "</myInt>";
				al.add(line);
				line = ""; 
			}
			if(fieldList[i].getType() == long.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myLong xsi:type=\"xsd:long\">" + invokeRet.toString() + "</myLong>";
				al.add(line); 
				line = "";
			}
			if(fieldList[i].getType() == String.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myString xsi:type=\"xsd:string\">" + invokeRet.toString() + "</myString>";
				al.add(line); 
				line = "";
			}
			if(fieldList[i].getType() == boolean.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myBool xsi:type=\"xsd:boolean\">" + invokeRet.toString() + "</myBool>";
				al.add(line); 
				line = "";
			}
		}
		al.add("</complexType>");
		al.add("</DPSerialization>");
		//write stuff to file
		fos = new FileOutputStream(fileName);
		oos = new ObjectOutputStream(fos);
		oos.writeObject(al);
	}
	
	public void writeObj(MyAllTypesSecond second, String format) {
		al.add("<DPSerialization>");
		al.add("<complexType xsi:type=\"genericCheckpointing.util.MyAllTypesSecond\">");
		String clsName = "MyAllTypesSecond";
		obj = clsName.newInstance();
		Class<?> MyAllTypesFirstClass = obj.getClass();
		Field[] fieldList = MyAllTypesFirstClass.getFields();
		//Constructor[] allConstructors = MyAllTypesFirstClass.getDeclaredConstructors();
		Class<?> [] argTypes = new Class<?>[4];
		argTypes[0] = double.class;
		argTypes[1] = float.class;
		argTypes[2] = short.class;
		argTypes[3] = char.class;
		Constructor constructor = MyAllTypesSecondClass.getDeclaredConstructor(argTypes);
		for(int i = 0; i < fieldList.length; i++) {
			Class fieldClass = fieldList[i].getType();
			String fieldName = fieldList[i].getName();
			Object fieldObject = fieldList[i].get(obj);
			
			if(fieldList[i].getType() == double.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myDoubleT xsi:type=\"xsd:double\">" + invokeRet + "</myDoubleT>";
				al.add(line); 
				line = "";
			}
			if(fieldList[i].getType() == float.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myFloatT xsi:type=\"xsd:float\">" + invokeRet + "</myFloatT>";
				al.add(line); 
				line = "";
			}
			if(fieldList[i].getType() == short.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myShortT xsi:type=\"xsd:float\">" + invokeRet + "</myFloatT>";
				al.add(line); 
				line = "";
			}
			if(fieldList[i].getType() == char.class) {
				//get field name
				String methodName = "get" + fieldName;
				Method getterMethod = MyAllTypesFirst.getMethod(fieldName);
				Object invokeRet = getterMethod.invoke(obj);
				line = line + "<myCharT xsi:type=\"xsd:char\">" + invokeRet + "</myCharT>";
				al.add(line); 
				line = "";
			}
		}
		al.add("</complexType>");
		al.add("</DPSerialization>");	
	//write stuff to file
	fos = new FileOutputStream(fileName);
	oos = new ObjectOutputStream(fos);
	oos.writeObject(al);
	
	}
}
*/
