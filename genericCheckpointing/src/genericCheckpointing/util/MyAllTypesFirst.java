package genericCheckpointing.util;
import genericCheckpointing.util.SerializableObject;
import java.lang.Object;
public class MyAllTypesFirst extends SerializableObject {
	private int myInt;
	private long myLong;
	private String myString;
	private boolean myBool;
	
	public MyAllTypesFirst() {}
	public MyAllTypesFirst(Integer i) {
		this.myInt = i;
		this.myLong = Long.valueOf(i);
		this.myString = Integer.toString(i);
		this.myBool = true;
		if(i % 2 == 1) {
			this.myBool = false;
		}
	}
	
	
	public void setInt(int intIn) {
		this.myInt = intIn;
		System.out.println("setInt was called" + intIn);
	}
	public void setLong(long longIn) {
		this.myLong = longIn;
		System.out.println("setLong was called" + longIn);
	}
	public void setString(String stringIn) {
		this.myString = stringIn;
		System.out.println("setString was called"+ stringIn);
	}
	public void setBool(boolean boolIn) {
		this.myBool = boolIn;
		System.out.println("setBool was called" + boolIn);
	}
	
	public int getmyInt() {
		return this.myInt;
	}
	public long getmyLong() {
		return this.myLong;
	}
	public String getmyString() {
		return this.myString;
	}
	public boolean getmyBool() {
		return this.myBool;
	}
}
	
