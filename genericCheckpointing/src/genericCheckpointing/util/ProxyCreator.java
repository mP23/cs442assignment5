package genericCheckpointing.util;
import genericCheckpointing.server.StoreRestoreI;
import java.lang.Object;
import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationHandler;

public class ProxyCreator {
	public StoreRestoreI createProxy(Class<?>[] interfaceArray, InvocationHandler handler) {
		StoreRestoreI storeRestoreRef = (StoreRestoreI) Proxy.newProxyInstance(getClass().getClassLoader(), interfaceArray, handler);
		return storeRestoreRef;
	}
}
