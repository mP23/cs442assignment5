Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml clean

## To compile: 
ant -buildfile src/build.xml all

## To run by specifying arguments from command line [similarly for the 2nd argument and so on ...]
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0=firstarg 

## To run by specifying args in build.xml (just for debugging, not for submission)
ant -buildfile src/build.xml run

## To create tarball for submission
ant -buildfile src/build.xml tarzip

[
  adopted from http://www.cs.rochester.edu/u/www/courses/171/Fall-03/files/readme.txt
  by Deger Cenk Erdil
  for CS654 Distributed Systems
  This is a template README file about how you should form your own README file.
  In general,
        you should remove anything in between square brackets (i.e. [..]), and
        you should replace anything in between <> with a value.
]

CS542 Design Patterns
Spring 2011
PROJECT 2 README FILE

Due Date: Sunday, May 8, 2016
Submission Date: Monday, May 16, 2016
Grace Period Used This Project: 0 Days
Grace Period Remaining: 0 Days
Author(s): Matt Price
e-mail(s): mprice3@binghamton.edu


PURPOSE:

The purpose of this project is to serialize objects to a file, and deserialize them using dynamic proxies and reflection. 


PERCENT COMPLETE:

I believe that this project is 90% complete. Everything compiles, but I can't seem to run the methods. Once the methods run, I'm pretty sure it would work.

PARTS THAT ARE NOT COMPLETE:

writeObj and readObj are not called for some reason...

BUGS:

None, it compiles

FILES:

README, the text file you are presently reading
MyAllTypes2.txt, a text file containing the xml file to be deserialized
build.xml, the ant build file to compile this program
Driver.java, the driver code associated with the program also contains main
MyAllTypesFirst.java, the java file containing setters and getters for various data fields 
MyAllTypesSecond.java, the java file containing setters and getters for various data fields
StoreRestoreHandler.java, the java file containing methods to open a file, deserialize objects, write objects to a file, and close the file
StoreRestoreI.java, an empty tag interface
StoreI.java, the interface containing declarations of writeObj, a method that serializes objects to a file
RestoreI.java, the interface containing a declaration of readObj, a method to deserialize an XML file
ProxyCreator.java, the java class that calls createProxy, which returns a dynamic proxy reference
SerializableObject.java, an empty base class


SAMPLE OUTPUT:

TO COMPILE:

Extract files and type "ant all" from the inside the src directory

TO RUN:

Compile and then type "ant run -Darg0=mode -Darg1=NUM_OF_ITERATIONS -Darg2=file"
MyAllTypes2.txt is in the top genericCheckpointing file and holds the XML file.

Vectors:

I chose a vector to store the data, because that was offered as a structure to use. 

EXTRA CREDIT:
N/A

BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.
Matt Price

ACKNOWLEDGEMENT:
None
